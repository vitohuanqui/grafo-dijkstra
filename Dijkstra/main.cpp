#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <stdlib.h>
#include <map>
#include <vector>
#include <utility>
#include <queue>
#include <functional>

#define MAY 1000000

using namespace std;

typedef map<int,vector<pair<int,int>>>::iterator iter;

int string_to_int(string str)
{
    return atoi(str.c_str());
}

vector<int> parceo (map<int,vector<pair<int,int>>> &grafo)
{
    ///Agregamos LOS NODOS///
    ///Cambiar a grafo1.txt para probar con el de 8 nodos///
    ///Cambiar a grafo2.txt para probar con el de 200 nodos///

    ifstream nodos("200.txt");
    int i,x,fi,se;
    char c;
    string cadena,first,second;
    vector<int> distancia;
    while(!nodos.eof())
    {
        nodos >> cadena;
        x = cadena.find(',');
        if(x==-1)
        {
            i = string_to_int(cadena);
            distancia.push_back(MAY);
        }
        else
        {
            first = cadena.substr(0,x);
            second = cadena.substr(x+1);
            fi = string_to_int(first);
            se = string_to_int(second);
            grafo[i].push_back(make_pair(fi,se));
        }
    }
    nodos.close();
    return distancia;
}

void print_graph(map<int,vector<pair<int,int>>> grafo)
{
    for(iter it=grafo.begin();it!=grafo.end();++it)
    {
        cout<<it->first<< " : ";
        for(int i=0;i!=it->second.size();i++)
        {
            cout<<it->second[i].first<<" - "<<it->second[i].second<<" ; ";
        }
    }
}


void trayectoria(int ini,int fin,int camino[])
{
    int i=fin-1;
    while(camino[i]!=ini)
    {
        cout<<camino[i]<< " - ";
        i=camino[i]-1;
    }
}

void print_distancia(vector<int> dis,int pos,int camino[],int ini)
{
    if(pos!=-1)
    {
        cout<<"Distancia al nodo "<< pos << " es " <<dis[pos-1]<<endl;
        cout<<"Con la trayectoria de : "<<endl;
        trayectoria(ini,pos,camino);
    }
    else
    {
        for(int i=0;i<dis.size();++i)
        {
            cout<<"Al nodo: " <<i << " distancia :"<< dis[i]<<endl;
        }
    }
}



void dijkstra(int bgn,int endi)
{
    ///--** INICIALIZACION **--///
    map<int,vector<pair<int,int>>> grafo; /// creamos un map de int de pair de ints
    vector<int> distancia; /// se crea un vector para guardar las distancias del nodo a los demas
    distancia=parceo(grafo); /// parseamos el grafo y lo guardamos, este grafo es pasado por referencia
    priority_queue<pair<int,int>> head; /// este es el head del std
    int actual=bgn; /// usaremos una variable auxiliar para guardar el nodo inicial
    int nodo,peso; /// tenemos el peso y los nodos
    int camino[grafo.size()]; /// Esta lista nos servira para guardar el camino mas corto a todos los nodos
    bool visitado[grafo.size()]; /// La lista de bool, nos ayudara para guardar si es que el nodo ya ha sido visitado
    iter it = grafo.find(actual); /// para recorrer el map, tenemos que hacerlo con un iterador, asi que la funcion find nos retorna el iterador en la posicion que buscamos
    for (int i=0;i<grafo.size();i++) /// Aca llenaremos las listas, el camino con 0 y todos los visitados con false
    {
        camino[i]=0;
        visitado[i]=false;
    }
    distancia[actual-1]=0; /// ahora cambiaremos la distancia con 0 del nodo inicial
    visitado[actual-1]=true; /// y tambien que ya fue visitado
    ///Dijkstra///
    while(1) /// entramos con un while(1), porque terminara cuando un nodo se quede sin aritas a que recorrer
    {
        vector<pair<int,int>> lista_head; /// este vector nos ayudara como un auxiliar del head
        for(int i=0;i!=it->second.size();i++)///el inicio con todos sus aristas
        {
            nodo=it->second[i].first;///Sacamos el nodo
            peso=it->second[i].second;///Sacamos la arista (peso)
            if(distancia[actual-1]+peso<distancia[nodo-1])/// Se escoge el de menor peso
                distancia[nodo-1] = distancia[actual-1]+peso;/// Se actualiza el peso menor
            head.push(make_pair(peso,nodo));/// Se agrega al heap, este nos lo ordena por el peso
        }
        while (!head.empty())
        {
            lista_head.push_back(make_pair(head.top().first,head.top().second)); /// Entonces ya al tenerlo ordenado, lo sacamos y lo guardamos en un vector
            head.pop();/// y lo eliminamos del head, para dejarlo sin elementos para el siguiente nodo
        }
        ///Nuevo actual
        while(visitado[lista_head.back().second-1]==true) /// En la lista sacamos el menor y preguntamos si es que ya fue visitado
        {
            lista_head.pop_back();/// si la fue visitado lo eliminamos
            if(lista_head.empty())/// Y si la lista queda sin elementos, es porque ya existen caminos porque recorrer y terminamos el algoritmo
            {
                print_distancia(distancia,endi,camino,bgn);/// Por ultimo imprimimos la distancia y la trayectoria
                return;
            }
        }
            /// si es que hay elementos
            camino[lista_head.back().second-1] = actual; /// debemos de actualizar el nuevo nodo con el actual
            actual=lista_head.back().second; /// actualizar el actual con el nuevo nodo
            visitado[actual-1]=true; /// y marcar como visitado al actual
            it = grafo.find(actual); /// sacamos el iterador del actual
    }
}
int main()
{
    cout<<"Dijkstra"<<endl;
    dijkstra(1,197);/// el distra le pasamos el nodo que queremos visitar y
    ///7,37,59,82,99,115,133,165,188,197.
    ///Distancia al nodo 7 es 2599
    ///Distancia al nodo 37 es 3684
    ///Distancia al nodo 59 es 2947
    ///Distancia al nodo 82 es 2052
    ///Distancia al nodo 99 es 2367
    ///Distancia al nodo 115 es 2399
    ///Distancia al nodo 133 es 2029
    ///Distancia al nodo 165 es 2442
    ///Distancia al nodo 188 es 3139
    ///Distancia al nodo 197 es 5592
}
